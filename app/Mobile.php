<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
class Mobile
{

	protected $provider;

	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall()->call();
	}

	public function sendSmSText($sms)
	{
		if( empty($sms) ) return;

		$response = ContactService::validateNumber($sms->getPhone());

		return $this->provider->sendSms($sms->getPhone());
	}


}
