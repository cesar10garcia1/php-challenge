<?php

namespace App\Services;
use App\Interfaces\CarrierInterface;
use App\Call;
use App\Contact;

// Implementar la interfaz
// Ésto funcionará
class CarrierService implements CarrierInterface
{
    protected $contact;
    public function dialContact(Contact $contact){
        $this->contact = $contact;
    }

	public function makeCall() : Call{
        return new Call();
    }

    public function sendSms($phone){
        return true;
    }
}