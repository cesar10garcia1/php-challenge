<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	public static function findByName(Contact $contact): Contact
	{
		return $contact;
	}

	public static function validateNumber(string $number): bool
	{
		return strlen($number) == 9 ? true : false;
	}
}