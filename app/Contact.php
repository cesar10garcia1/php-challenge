<?php

namespace App;


class Contact
{
	protected $name ;
	protected $phone;
	function __construct($name, $phone)
	{
		$this->name = $name;
		$this->phone = $phone;
	}
	function getName(){
		return $this->name;
	}
	function getPhone(){
		return $this->phone;
	}
}