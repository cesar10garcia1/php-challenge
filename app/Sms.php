<?php

namespace App;


class Sms
{
	protected $phone ;
	protected $message;
	function __construct($phone, $message)
	{
		$this->phone = $phone;
		$this->message = $message;
	}
	function getMessage(){
		return $this->message;
	}
	function getPhone(){
		return $this->phone;
	}
}