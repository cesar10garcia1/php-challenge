<?php declare(strict_types=1);

namespace Tests;

use App\Mobile;
use App\Contact;
use App\Sms;
use App\Services\CarrierService;
use App\Services\ContactService;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class MobileTest extends TestCase
{

	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$contact = new Contact('Cesar','969641315');
		$carrier = new CarrierService($contact);
		$mobile = new Mobile($carrier);

		$this->assertEquals(
			'llamada exitosa',
			$mobile->makeCallByName($contact)
		);
	}
	/** @test */
	public function it_validate_numbre_cellphone()
	{
		$response = ContactService::validateNumber('969641315');
		$this->assertTrue($response);
	}

	/** @test */
	public function send_sms_text()
	{
		$contact = new Contact('Cesar','969641315');
		$carrier = new CarrierService($contact);
		$sms = new Sms($contact->getPhone(), 'Hola Cesar');
		$response = new Mobile($carrier);
		$this->assertTrue( $response->sendSmSText($sms) );
	}

}
